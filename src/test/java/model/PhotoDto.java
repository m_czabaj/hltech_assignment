package model;

import com.google.gson.Gson;

public class PhotoDto {

    private int albumId;
    private int id;
    private String title;
    private String url;
    private String thumbnailUrl;

    /*
     * Getters
     */

    public int albumId() {
        return albumId;
    }

    public int id() {
        return id;
    }

    public String title() {
        return title;
    }

    public String url() {
        return url;
    }

    public String thumbnailUrl() {
        return thumbnailUrl;
    }

    /*
     * Setters
     */

    public PhotoDto withAlbumId(int albumId) {
        this.albumId = albumId;
        return this;
    }

    public PhotoDto withId(int id) {
        this.id = id;
        return this;
    }

    public PhotoDto withTitle(String title) {
        this.title = title;
        return this;
    }

    public PhotoDto withUrl(String url) {
        this.url = url;
        return this;
    }

    public PhotoDto withThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return this;
    }

    /*
     * Misc
     */

    @Override
    public boolean equals(Object o){
        PhotoDto photo = (PhotoDto)o;
        return this.albumId == photo.albumId
                && this.id == photo.id
                && this.thumbnailUrl.equals(photo.thumbnailUrl)
                && this.title.equals(photo.title)
                && this.url.equals(photo.url);
    }

    /**
     * Method could be used as serializer as well.
     *
     * @return
     */
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
