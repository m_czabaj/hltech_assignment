package listeneres;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Arrays;

public class TestListener implements ITestListener {

    Logger logger = LoggerFactory.getLogger(TestListener.class);

    @Override
    public void onTestStart(ITestResult iTestResult) {
        logger.info("Start test " + iTestResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logger.info("Test " + iTestResult.getName() + " succeeded");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logger.info("Test " + iTestResult.getName() + " failed. Test data: " + Arrays.asList(iTestResult.getParameters()));
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        logger.info("Test " + iTestResult.getName() + " skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
