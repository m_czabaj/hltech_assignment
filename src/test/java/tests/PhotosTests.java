package tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import config.Configuration;
import data_providers.PhotosDataProvider;
import listeneres.TestListener;
import model.PhotoDto;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

@Listeners(TestListener.class)
public class PhotosTests {

    private String photosEndpoint = String.format("%s://%s/photos", Configuration.appProtocol, Configuration.appHost);

    /**
     * Method provides that there always will be at least one entity in DB before tests. In case of https://jsonplaceholder.typicode.com/photos
     * is not applicable because some predefined content already exists and new entities are not saved at all.
     */
    @BeforeClass
    public void addPhotoForTests() {
        PhotoDto testPhoto = new PhotoDto()
                .withAlbumId(1)
                .withThumbnailUrl("https://thumbnail.test.url.io")
                .withTitle("Photo added before /photo endpoint tests")
                .withUrl("https://test.url.io");

        Gson gson = new Gson();
        Response response = RestAssured.given()
                .body(testPhoto.toString())
                .post(photosEndpoint);

        assertThat(response.statusCode(), equalTo(201));
        PhotosDataProvider.photosForTests.add(gson.fromJson(response.asString(), PhotoDto.class));
    }

    /**
     * Cleaning up after tests.
     */
    @AfterClass(enabled = false)
    public void cleanUp(){
        for(PhotoDto testPhoto : PhotosDataProvider.photosForTests){
            RestAssured.delete(photosEndpoint + "/" + testPhoto.id());
        }
    }

    /* ------------------------------- TESTS ------------------------------- */

    /**
     * Test: GET /photos
     */
    @Test
    public void getAllPhotos() {
        Response response = RestAssured.get(photosEndpoint);
        String responseBody = response.asString();
        Gson gson = new Gson();
        List<PhotoDto> photos = gson.fromJson(responseBody, new TypeToken<List<PhotoDto>>(){}.getType());

        assertThat(response.statusCode(), equalTo(200));
        assertThat(photos.size(), greaterThanOrEqualTo(1)); // must be greater than 0 because one photo has been added before tests
    }

    /**
     * Test: GET /photos?{parameter}
     */
    @Test(
            dataProviderClass = PhotosDataProvider.class,
            dataProvider = "dataForParametersTests"
    )
    public void parametersTests(PhotoDto testPhoto, String parameterName, Object parameterValue) {
        Gson gson = new Gson();

        // get base list
        String baseResponseBody = RestAssured.given()
                .parameter(parameterName, parameterValue)
                .get(photosEndpoint)
                .asString();
        List<PhotoDto> baseFilteredPhotos = gson.fromJson(baseResponseBody, new TypeToken<List<PhotoDto>>(){}.getType());

        // add new photo
        String newPhotoData = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(testPhoto.toString())
                .put(photosEndpoint + "/" + testPhoto.id())
                .asString();
        PhotoDto newPhoto = gson.fromJson(newPhotoData, PhotoDto.class);

        // get new list
        Response newResponse = RestAssured.given()
                .parameter(parameterName, parameterValue)
                .get(photosEndpoint);
        String newResponseBody = newResponse.asString();
        List<PhotoDto> newFilteredPhotos = gson.fromJson(newResponseBody, new TypeToken<List<PhotoDto>>(){}.getType());

        assertThat(newResponse.statusCode(), equalTo(200));
        /* If API could save the new data, this assertion would be uncommented.
         * baseFilteredPhotos.add(newPhoto);
         * assertThat(newFilteredPhotos, equalTo(baseFilteredPhotos));
         */
    }

    /**
     * Test: GET /photos/{id}
     */
    @Test(
            dataProviderClass = PhotosDataProvider.class,
            dataProvider = "getTestPhoto"
    )
    public void getSpecificPhoto(PhotoDto testPhoto) {
        Response response = RestAssured.get(photosEndpoint + "/" + testPhoto.id());
        String responseBody = response.asString();
        Gson gson = new Gson();
        PhotoDto obtainedPhoto = gson.fromJson(responseBody, PhotoDto.class);

        assertThat(response.statusCode(), equalTo(200));
        assertThat(obtainedPhoto, equalTo(testPhoto));
    }

    /**
     * Test: PUT /photos/{id}
     */
    @Test(
            dataProviderClass = PhotosDataProvider.class,
            dataProvider = "getTestPhoto"
    )
    public void editSpecificPhoto(PhotoDto testPhoto) {
        String newTitle = String.valueOf(System.currentTimeMillis());
        testPhoto.withTitle(newTitle);

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(testPhoto.toString())
                .put(photosEndpoint + "/" + testPhoto.id());

        String responseBody = response.asString();
        Gson gson = new Gson();
        PhotoDto obtainedPhoto = gson.fromJson(responseBody, PhotoDto.class);

        assertThat(response.statusCode(), equalTo(200));
        assertThat(obtainedPhoto, equalTo(testPhoto));
        // in fact here should be made another GET request to /photos/testPhoto.id() to check if API returns correct data
    }

    /**
     * Test: POST /photos
     * @param albumID
     * @param title
     * @param url
     * @param thumbnailUrl
     */
    @Test(
            dataProviderClass = PhotosDataProvider.class,
            dataProvider = "dataForNewPhoto"
    )
    public void addPhoto(int albumID, String title, String url, String thumbnailUrl) {
        PhotoDto testPhoto = new PhotoDto()
                .withAlbumId(albumID)
                .withThumbnailUrl(title)
                .withTitle(url)
                .withUrl(thumbnailUrl);

        Gson gson = new Gson();
        Response response = RestAssured.given()
                .body(testPhoto.toString())
                .post(photosEndpoint);

        assertThat(response.statusCode(), equalTo(201));
        PhotosDataProvider.photosForTests.add(gson.fromJson(response.asString(), PhotoDto.class));
        // here should be made next GET request to /photos/{newAddedPhoto.id} to check if API returns correct data
    }

    /**
     * Test: DELETE /photos/{id}
     */
    @Test(
            dataProviderClass = PhotosDataProvider.class,
            dataProvider = "getTestPhoto",
            priority = 1 // to provide that this test wil be executed as last
    )
    public void deleteSpecificPhoto(PhotoDto testPhoto) {
        Response response = RestAssured.delete(photosEndpoint + "/" + testPhoto.id());

        assertThat(response.statusCode(), equalTo(200));
        // here should be made another GET request to /photos/testPhoto.id() to check if API returns 404
    }





}
