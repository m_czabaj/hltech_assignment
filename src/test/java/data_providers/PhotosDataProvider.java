package data_providers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import config.Configuration;
import model.PhotoDto;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PhotosDataProvider {

    private final String photosEndpoint = String.format( "%s://%s/photos", Configuration.appProtocol, Configuration.appHost);
    public static List<PhotoDto> photosForTests = new ArrayList<>();

    @DataProvider(name = "getAlbumsIds")
    public Object[][] getAlbumsIds(){
        String json = RestAssured.get(photosEndpoint).asString();

        Gson gson = new Gson();
        List<PhotoDto> photos = gson.fromJson(json, new TypeToken<List<PhotoDto>>(){}.getType());

        return new Object[][]{
                { photos.stream().map(PhotoDto::albumId).collect(Collectors.toList()) }
        };
    }

    @DataProvider(name = "getTestPhoto")
    public Object[][] getTestPhoto(){
        PhotoDto mockPhoto = new PhotoDto()
                .withAlbumId(1)
                .withId(1)
                .withTitle("accusamus beatae ad facilis cum similique qui sunt")
                .withUrl("https://via.placeholder.com/600/92c952")
                .withThumbnailUrl("https://via.placeholder.com/150/92c952");

        return new Object[][]{
                // { photosForTests.get(0)} -> this should be returned but API does not save any new photos
                { mockPhoto }
        };
    }

    @DataProvider(name = "dataForParametersTests")
    public Object[][] dataForParametersTests(){
        PhotoDto basePhoto = new PhotoDto();
        int albumId = 1;
        String title = "accusamus beatae ad facilis cum similique qui sunt";
        String url = "https://via.placeholder.com/600/92c952";
        String thumbnailUrl = "https://via.placeholder.com/150/92c952";

        return new Object[][]{
                /* {photoDto, parameter name, parameter value} */
                { basePhoto.withTitle(title), "title", title },
                { basePhoto.withAlbumId(albumId), "albumId", albumId},
                { basePhoto.withUrl(url), "url", url},
                { basePhoto.withThumbnailUrl(thumbnailUrl), "thumbnailUrl", thumbnailUrl}
        };
    }

    @DataProvider(name = "dataForNewPhoto")
    public Object[][] dataForNewPhoto(){
        return new Object[][]{
                {1, "simple title", "https://simple.url.io", "https://thumbnail.url.io"}
        };
    }


}
